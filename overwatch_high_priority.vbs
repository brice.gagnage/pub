Const HIGH_PRIORITY= 128
 
strComputer = "." 
Set objWMIService = GetObject("winmgmts:" _ 
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
 
Set colProcesses = objWMIService.ExecQuery _ 
    ("Select * from Win32_Process Where Name = 'overwatch.exe'") 
 
For Each objProcess in colProcesses 
    objProcess.SetPriority(HIGH_PRIORITY)  
Next 