# GUITask Hero
You are being asked to implement a scheduler for your threads. You are given a text file as input for your program which defines the order of execution of some tasks. You must create those tasks and run them on your threads according to the order in the file.

## Your program job
- read the file and its elements appropriately.
- create a counter accessible by all threads, initialized by 0.
- start N threads, one assigned to each column.
- for each 'X' on each line from top to bottom, run the task in its thread.
- print the counter value

## The task to be run
- generate a random number between 0 and 100
- add this number to the counter accessible by all threads
- print the current thread number(Thread 1, Thread 2, Thread 3...) and the random generated number

## Input file format
**N** *-> total number of threads*  
**|1|2|3|** *-> the thread numbers*  
**|X| | |** *-> the thread from where the printing must be done (here, thread 1)*  

> **Note:** The input file format can be considered as always correct. 

## Expectations: 
- the order of execution of tasks must respect the order of the lines
- the faster the better !

> **Tip:** You can use any framework you want to help you. The UI is expected to be simple (console display is enough), but you can chose whichever you want to use. Keep in mind we will pay more attention on the algorithm part rather than cosmetics.

## Example
**Input file**
```
3
|1|2|3|
|X| | |
| |X| |
|X| | |
| | |X|
|X| | |
| |X| |
```
**Expected program behavior**
- thread 1 prints "Thread 1 - generated number: 42"
- THEN, thread 2 prints "Thread 2 - generated number: 51"
- THEN, thread 1 prints "Thread 1 - generated number: 46"
- THEN, thread 3 prints "Thread 3 - generated number: 34"
- THEN, thread 1 prints "Thread 1 - generated number: 92"
- THEN, thread 2 prints "Thread 2 - generated number: 1"
- main thread prints "TOTAL: 266"

> **WARNING:** be careful to respect the order of execution ! It must match the order in the file !

```mermaid
sequenceDiagram                                                                                                                                                                             participant T1 as Thread 1
    participant T2 as Thread 2
    participant T3 as Thread 3
    Note over T1: run task
    T1 ->> T2: 
    Note over T2: run task
    T2 ->> T1: 
    Note over T1: run task
    T1 ->> T3: 
    Note over T3: run task
    T3 ->> T1: 
    Note over T1: run task
    T1 ->> T2: 
    Note over T2: run task
    
```

**Expected displayed result**
```
Thread 1 - generated number: 42
Thread 2 - generated number: 51
Thread 1 - generated number: 46
Thread 3 - generated number: 34
Thread 1 - generated number: 92
Thread 2 - generated number: 1
TOTAL: 266
```


