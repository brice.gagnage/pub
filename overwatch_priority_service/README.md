Description
-------
This script allows a user to install a Windows service that automatically sets
overwatch.exe instances to a 'High' priority on creation.

This aims to increase performance in-game, especially when running other
programs (IRC, Dicord, video recorder...) at the same time.

Prerequisites
-------
- Windows 7 or superior
- A user with administrative rights

How to install
-------
- Open a Powershell as administrator
- Change the path to the location of your script
``` PS> cd <your_path> ```
- Install the service:
``` PS> .\ow_priority_service.ps1 -Setup ```
- Start the service:
``` PS> ow_priority_service.ps1 -Start ```

The service will be started automatically at start-up.

How to uninstall
-------
- Open a Powershell as administrator
- Change the path to the location of your script
``` PS> cd <your_path> ```
- Stop the service:
``` ow_priority_service.ps1 -Stop ```
- Uninstall the service with this command line:
``` ow_priority_service.ps1 -Remove ```

Authors
-------
- Initial service template: JFLarvoire (https://github.com/JFLarvoire/SysToolsLib)
- Overwatch service: Brice Gagnage

License
-------

Copyright 2019 Brice Gagnage

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
